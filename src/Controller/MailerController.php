<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use App\Repository\SiteRepository;
use App\Entity\User;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;


class MailerController extends AbstractController
{
    /**
     * @Route("/mailer", name="mailer")
     */
    public function sendEmail(MailerInterface $mailer, SiteRepository $siteRepository, Request $request, UserInterface $user)
    {
        $userId = $user->getId();
        $user = $this->getDoctrine()->getRepository(User::class)->find($userId);

        $sites = $user->getSites();
        foreach($sites as $site)
        {
            $status = $site->getStatus();
            try{
                    $client = HttpClient::create();
                    $urlSite = $site->getUrl();
                    $response = $client->request('GET', $site->getUrl());
                    $statusCode = $response->getStatusCode();
                    $site->setStatus($statusCode);
                    // Send an email if the status code of the web site is 503
                    if($statusCode == 503)
                    {
                        $email = (new Email())
                        ->from('chekini_hakima@yahoo.fr')
                        ->to($user->getEmail())
                        ->subject('Your site is Offline')
                        ->text('This is a notification from your the web site monotoring app. One of your web sites is offline, the site\'s url is: ' . $site->getUrl())
                        ->html('<p>This is a notification from your the web site monotoring app. One of your sites is offline </p>');
                        $mailer->send($email);
                        
                        $request->getSession()->getFlashBag()->add('danger', 'Email sent, one of your sites is offline');
                        return $this->redirectToRoute('site_index');
                        
                    }
                } catch(\Exception $e){
                    $request->getSession()->getFlashBag()->add('danger', $e->getMessage());
                    return  $this->render('site/index.html.twig', ['sites'=>$sites]);
                } 
        }
        $request->getSession()->getFlashBag()->add('success', "All your websites are online");
        return  $this->redirectToRoute('site_index', ['sites'=>$sites]);
    }
}
