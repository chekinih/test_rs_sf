<?php

namespace App\Controller;

use App\Entity\Site;
use App\Form\SiteType;
use App\Repository\SiteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\User;

/**
 * @Route("/site")
 */
class SiteController extends AbstractController
{
    /**
     * @Route("/", name="site_index", methods={"GET"})
     */
    public function index( UserInterface $user): Response
    {
        $userId = $user->getId();
        $user = $this->getDoctrine()->getRepository(User::class)->find($userId);
        $sites = $user->getSites();
        // The user won t acces to this page until he is authenticated
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        return $this->render('site/index.html.twig', [
            'sites' => $sites,
        ]);
    }

    /**
     * @Route("/new", name="site_new", methods={"GET","POST"})
     */
    public function new(Request $request, UserInterface $user): Response
    {
        
        $site = new Site();
        $form = $this->createForm(SiteType::class, $site);
        $form->handleRequest($request);

        $userId = $user->getId();
        $user = $this->getDoctrine()->getRepository(User::class)->find($userId);

        if ($form->isSubmitted() && $form->isValid()) {
            try{
            $client = HttpClient::create();
            $urlSite = $site->getUrl();
                $response = $client->request('GET', $site->getUrl());
                $statusCode = $response->getStatusCode();
                $site->setStatus($statusCode);
                $user->addSite($site);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($site);
                $entityManager->flush();
            } catch(\Exception $e){
                $request->getSession()->getFlashBag()->add('danger', "Check out the url of your web site");
                return $this->redirectToRoute('site_new', [ 'site' => $site,'form' => $form->createView(),]);
            }
            return $this->redirectToRoute('site_index');
            }

        return $this->render('site/new.html.twig', [
            'site' => $site,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="site_show", methods={"GET"})
     */
    public function show(Site $site): Response
    {
        return $this->render('site/show.html.twig', [
            'site' => $site,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="site_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Site $site): Response
    {
        $form = $this->createForm(SiteType::class, $site);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try{
                $client = HttpClient::create();
                $urlSite = $site->getUrl();
                    $response = $client->request('GET', $site->getUrl());
                    $statusCode = $response->getStatusCode();
                    $site->setStatus($statusCode);
                } catch(\Exception $e){
                    $request->getSession()->getFlashBag()->add('danger', "Check out the url of your web site");
                    return $this->render('site/edit.html.twig', [ 'site' => $site,'form' => $form->createView(),]);
                }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('site_index');
        }

        return $this->render('site/edit.html.twig', [
            'site' => $site,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="site_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Site $site): Response
    {
        if ($this->isCsrfTokenValid('delete'.$site->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($site);
            $entityManager->flush();
        }

        return $this->redirectToRoute('site_index');
    }
}
