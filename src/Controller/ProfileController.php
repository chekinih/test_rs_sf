<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use App\Form\UserType;

class ProfileController extends AbstractController
{
    /**
     * @Route("/profile", name="profile")
     */
    public function profile(UserInterface $user)
    {
        $userId = $user->getId();
        $user = $this->getDoctrine()->getRepository(User::class)->find($userId);
        return $this->render('profile/index.html.twig', ['user'=>$user
        ]);
    }

    /**
     * @Route("/profile/edit", name="edit_profile")
     */
    public function editProfile(Request $request, UserInterface $user)
    {
        try{
            $userId = $user->getId();
            $user = $this->getDoctrine()->getRepository(User::class)->find($userId);
            if (!$user) {
                throw $this->createNotFoundException("User with id: $id does not exist");
            }
            // Create a user form
            $form = $this->createForm(UserType::class, $user);
            $form->remove('password');
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $this->getDoctrine()->getManager()->flush();

                // Display a success message
                $request->getSession()->getFlashBag()->add('success', 'Your profile has been edited');
        
                return $this->redirectToRoute('profile', array('user' => $user));
            }
        } catch(\Exception $e){
            $request->getSession()->getFlashBag()->add('danger', $e->getMessage());
            return $this->redirectToRoute('site_index');
        }
        return $this->render('profile/edit.html.twig', ['form' => $form->createView()]);
    }
}

